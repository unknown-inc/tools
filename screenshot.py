import requests

sites = [
    'www.amazon.in',
    'www.flipkart.com',
    'www.nearbuy.com',
    'www.paytm.com',
    'www.askmebazaar.com',
    'www.shopclues.com',
    'www.snapdeal.com',
    'www.yepme.com',
    'www.jabong.com',
    'www.fashionandyou.com',
    'www.fashionara.com',
    'www.homeshop18.com',
    'www.zotezo.com',
    'shopping.indiatimes.com',
    'www.bata.in',
    'www.trendin.com',
    'www.americanswan.com',
    'www.abof.com',
    'www.Bestlylish.com,'
    'www.myntra.com',
    'www.ebay.in',
    'www.shopping.rediff.com'
]
for index, site in enumerate(sites):
    print(index)
    print(site)
    r = requests.get('http://localhost:3000/screen', {'page_id': index, 'url': site})
    print(r.content)
