import time
import requests

pages = ['/hello', '/', '/g-new-page', '/yes-you-can']

payload = {'uri': None}

userAgents = ['Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:68.0) Gecko/20100101 Firefox/68.0',
              'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.97 Safari/537.11',
              'Mozilla/5.0 (Linux; Android 4.0.4; Galaxy Nexus Build/IMM76B) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/18.0.1025.133 Mobile Safari/535.19']
headers = {
    'Authorization': 'Bearer ys4SCznNV00piV007n4AuA',
    'Content-Type': 'application/json'
}
success = 0
failures = 0
for i in range(0, 1):
    headers['User-Agent'] = userAgents[i % 2]
    headers['site_key'] = userAgents[i % 2]
    payload['uri'] = pages[2]  # pages[random.randint(0, 3)]

    r = requests.post("http://localhost:3002/api/track", json=payload, headers=headers)
    if r.status_code == 200:
        success += 1
        print('sucess')
    elif r.status_code == 429:
        failures += 1
    else:
        pass
        print(r.status_code)
        print(r.text)
    time.sleep(.100)

print("sucess : {}".format(success))
print("failures : {}".format(failures))
